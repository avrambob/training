/**
 * Created by avram on 04-May-20.
 */

trigger AddRelatedRecord on Account (after insert, after update) {

    List<Opportunity> oppList = new List<Opportunity>();

    //Get the related opportunities for the accounts in this trigger

    Map<Id, Account> acctsWithOpps = new Map<Id, Account>([SELECT Id,(SELECT Id FROM Opportunities) from Account where Id IN :Trigger.new]);

    System.debug('acctsWithOpps = '+ acctsWithOpps);
    System.debug('Trigger.New = '+ Trigger.new);
    System.debug('Trigger.newMap = ' + Trigger.newMap);
    
    // Add an opportunity for each account if it doesn't already have one.
    // Iterate through each account.

    for (Account a: Trigger.new){
        System.debug('acctsWithOpps.get(a.Id).Opportunities.size()=' + acctsWithOpps.get(a.Id).Opportunities.size());
        if(acctsWithOpps.get(a.Id).Opportunities.size() ==0){
            oppList.add(new Opportunity(Name=a.Name + ' Opportunity',
                    StageName='Prospecting',
                    CloseDate=System.today().addMonths(1),
                    AccountId=a.Id));
        }
        System.debug('acctsWithOpps.get(a.Id).Opportunities ' + acctsWithOpps.get(a.Id).Opportunities);
    }
    if(oppList.size() > 0){
        insert oppList;
    }

}
/**
 * Created by avram on 08-Oct-20.
 */

public with sharing class SoqlTraining {
// with sharing tine cont de sharing rules

    public static void workWithSOQL() {
        List<Account> accList = [SELECT Id,Name,Phone,Website from Account WHERE Id = '0012o00002NzRiXAAV'];

        for (Account a : accList) {
            System.debug(a);
        }

        List<Account> accListWithOpp = [
                SELECT Id,Name,Phone,Website, (SELECT Id,Name,CloseDate FROM Opportunities)
                from Account
                WHERE Id = '0012o00002NzRiXAAV'
        ];

        for(Account ac : accListWithOpp){
            System.debug(ac + ' + ' + ac.Opportunities + ' size ' + ac.Opportunities.size());
        }

    }
}
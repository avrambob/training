/**
 * Created by avram on 27-Feb-21.
 */

@IsTest
private class AccountProcessorTest2 {

    @TestSetup
    static void loadData(){
        List<Account> acList = new List<Account>();
        List<Contact> contactsList = new List<Contact>();

        for(Integer i=1;i<=10;i++){
            Account a = new Account(Name = 'Test ' + i);
            acList.add(a);
        }

        insert acList;

        for(Account a : acList) {
            for (Integer i = 1; i <= 2; i++) {
                Contact c = new Contact();
                c.FirstName = 'Test';
                c.LastName = 'C' + i;
                c.AccountId = a.Id;
                contactsList.add(c);
            }
        }

        insert contactsList;
    }

    @IsTest
    static void testBehavior() {
        Map<Id,Account> accMap = New Map<Id, Account>([SELECT Id FROM Account LIMIT 10]);
        List<Id> idList = new List<Id>(accMap.keySet());
        AccountProcessor2.countContacts2(idList);
    }
}
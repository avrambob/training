/**
 * Created by avram on 26-Jan-21.
 */

public with sharing class DailyLeadProcessor implements Schedulable{


    public void execute(SchedulableContext ctx) {
        List<Lead> myList = [SELECT Id,LeadSource FROM Lead WHERE LeadSource = null LIMIT 200];
        List<Lead> updatedList = new List<Lead>();

        for(Lead l : myList){
            l.LeadSource = 'Dreamforce';
            updatedList.add(l);
        }
        if(updatedList.size() > 0){
            update updatedList;
        }
    }
}
/**
 * Created by avram on 03-Feb-21.
 */

public with sharing class MetadataExample {
    public void  updateMetadata(){
        Metadata.CustomMetadata customMetadata = new Metadata.CustomMetadata();
        customMetadata.fullName = 'MyNamespace__MyMetadataTypeName.MyMetadataRecordName';
        Metadata.CustomMetadataValue customField = new Metadata.CustomMetadataValue();
        customField.field = 'customField__c';
        customField.value = 'New value';
        customMetadata.values.add(customField);
        Metadata.DeployContainer deployContainer = new Metadata.DeployContainer();
        deployContainer.addMetadata(customMetadata);

        Id asyncResultId = Metadata.Operations.enqueueDeployment(deployContainer, null);
    }
}

//Create a public Apex class called MetadataExample.
//Create a public method called updateMetadata that takes no arguments and returns void.
//Create an object called customMetadata of type Metadata.CustomMetadata.
//Set the fullName of the customMetadata object to be: 'MyNamespace__MyMetadataTypeName.MyMetadataRecordName'.
//Create an object called customField of the type Metadata.CustomMetadataValue.
//Set customField.field to 'customField__c'. Set customField.value to 'New value'.
//Add the customField object to the list of values in customMetadata.
//Create an object called deployContainer of type Metadata.DeployContainer.
//Add customMetadata to deployContainer.
//Use the Metadata.Operations.enqueueDeployment method to deploy the metadata.
//Assign the return value of the method to a variable called asyncResultId of type Id. Use null for the callback parameter.
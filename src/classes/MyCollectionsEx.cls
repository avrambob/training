/**
 * Created by avram on 20-Oct-20.
 */

public with sharing class MyCollectionsEx {

    public static void MyListDisplayEx(){
        List<Account> accList = [SELECT ID,Name,Phone,AnnualRevenue FROM Account LIMIT 10];
        for(Account a : accList){
            System.debug(a);
        }
    }

    public static void MySobjList(String phoneNo){

        List<Contact> contacts = new List<Contact>();
        List<Lead> leads = new List<Lead>();
        List<Account> accounts = new List<Account>();

        List<List<SObject>> results = [FIND :phoneNo IN Phone FIELDS
                                       RETURNING Contact(Id,Phone,FirstName,LastName),
                                        Lead(Id,Phone,FirstName,LastName),
                                        Account(Id,Phone,Name)];
        System.debug('Size lista de liste: ' + results.size());
        System.debug('Lista de liste de sObject: ' + results);
        List<SObject> records = new List<SObject>();
        records.addAll(results[0]);
        records.addAll(results[1]);
        records.addAll(results[2]);

        System.debug('Size lista sobj ' + records.size());
        System.debug('List de sObject: ' + records);

        if(!records.isEmpty()){
            for(Integer i=0;i<records.size();i++){
                SObject record = records[i];
                if(record.getSObjectType() == Contact.SObjectType){
                    contacts.add((Contact) record);
                } else if(record.getSObjectType() == Lead.SObjectType){
                    leads.add((Lead) record);
                } else if(record.getSObjectType() == Account.SObjectType){
                    accounts.add((Account) record);
                }
            }
        }
        System.debug('Contacts: ' + contacts);
        System.debug('Lead: ' + leads);
        System.debug('Account: ' + accounts);

        // echivalent
        /*if(!results.isEmpty()){
            for(List<sObject> myList : results){
                for(Integer i=0;i<myList.size();i++){
                    SObject record = myList[i];
                    if(record.getSObjectType() == Contact.SObjectType){
                        contacts.add((Contact) record);
                    } else if(record.getSObjectType() == Lead.SObjectType){
                        leads.add((Lead) record);
                    } else if(record.getSObjectType() == Account.SObjectType){
                        accounts.add((Account) record);
                    }
                }
            }


       } */
    }

    public static void MyMapEx(){
        Map<Id, Account> m = new Map<Id, Account>([SELECT  Name,Phone FROM Account LIMIT 10]);
        System.debug('Map este :' + m);
        System.debug('Values are:' + m.values());
        for(Id id : m.keySet()){
            System.debug(m.get(id));
            System.debug('Telefon: ' + m.get(id).Phone);
        }
    }

    public static void ParcurgereMape(){

        Map<Id, Account> acctMap = new Map<Id, Account>([SELECT Name,Phone FROM Account WHERE Phone != null LIMIT 10]);
        List<Account> accList = [SELECT Id FROM Account LIMIT 10];

        System.debug('Mapa: ' + acctMap);
        System.debug('Lista: ' + accList);

        for(Account a : accList){
            if(acctMap.containsKey(a.Id)){
                System.debug(a.id + ' se gaseste in lista! ' + acctMap.get(a.Id).Name + ' '  + acctMap.get(a.Id).Phone);
            }
        }

    }

}
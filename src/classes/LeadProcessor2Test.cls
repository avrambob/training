/**
 * Created by avram on 27-Feb-21.
 */

@IsTest
private class LeadProcessor2Test {

    @TestSetup
    static void loadData(){
        List<Lead> myList = new List<Lead>();
        for(Integer i= 1;i<201;i++){
            Lead l = new Lead(LastName = 'LeadLastName ' + i,Company = 'Company ' +i);
            myList.add(l);
        }
        insert  myList;
    }

    @IsTest
    static void testBehavior() {

        Test.startTest();
        LeadProcessor2 lp = new LeadProcessor2();
        Id batchId = Database.executeBatch(lp);
        Test.stopTest();
    }
}
/**
 * Created by avram on 07-Feb-21.
 */

@IsTest
private class AnimalLocatorTest {
    @IsTest
    static void testBehavior() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        String expectedValue = AnimalLocator.getAnimalNameById(2);
        System.debug('expectedValue=' + expectedValue);
    }
}
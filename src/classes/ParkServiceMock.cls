/**
 * Created by avram on 07-Feb-21.
 */
@IsTest
global class ParkServiceMock implements WebServiceMock{

    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {

        ParkService.byCountryResponse response_x = new ParkService.byCountryResponse ();
        //String s = 'Japan';
        response_x.return_x = new List<String>{'Shiretoko National Park', 'Oze National Park', 'Hakusan National Park'};
        // endx
        response.put('response_x',response_x);
    }
}
/**
 * Created by avram on 07-Feb-21.
 */
@isTest
    global class AnimalLocatorMock implements HttpCalloutMock{
    // Implement this interface method
        global HttpResponse respond(HttpRequest request) {
// Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"animal":{"id":2,"name":"bear","eats":"berries, campers, adam seligman","says":"yum yum"}}');
            response.setStatusCode(200);
            return response;

        }
}
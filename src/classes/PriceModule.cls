/**
 * Created by avram on 02-Nov-20.
 */

public with sharing class PriceModule {
// 
    public Decimal getRoomsPrices(Id roomId, Date fromDate, Date toDate){
//List<Season__c> mySelList = [Select Start__c,End__C,(SELECT Price_Applied__c FROM Prices__r) from Season__c WHERE Room__c =:roomId];
       // List<Price__c> myPricesList = [SELECT Price_Applied__c,Season__r.Start__c,Season__r.End__c FROM Price__c WHERE Room__c =:roomId];
       // for(Price__c p : myPricesList){
       //     if(fromDate >= p.Season__r.Start__c && toDate <= p.Season__r.End__c){
       //         priceToList.add(p);
       //     }
       // }
        Decimal lowestPrice;
        List<Decimal> priceToList = new List<Decimal>();
        List<Season__c> mySeasonPricesList = [SELECT Start__c,End__c,Price_Applied__c FROM Season__c WHERE Room__c =:roomId];

        for(Season__c s : mySeasonPricesList){
            if((fromDate >= s.Start__c && fromDate <= s.End__c) || (toDate >= s.Start__c && toDate <= s.End__c)){
                priceToList.add(s.Price_Applied__c);
            }
        }
        System.debug(priceToList);
        if(!priceToList.isEmpty()){
            priceToList.sort();
            lowestPrice = priceToList.get(0) * fromDate.daysBetween(toDate);
        }
        return lowestPrice;
    }
}
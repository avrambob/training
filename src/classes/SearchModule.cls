/**
 * Created by avram on 01-Nov-20.
 */

public with sharing class SearchModule {
    public List<Room__c> searchRoomByHotelName(String s){
        String stringToSearch = '%' + s + '%';
        //List<Room__c> roomsReturned = [SELECT Id,(SELECT Name FROM Rooms__r) FROM Hotel__c WHERE Name LIKE :stringToSearch AND Is_Active__c = TRUE].Rooms__r;
        //List<Room__c> roomsReturned = [SELECT Id,(SELECT Name from Rooms__r) from Hotel__C WHERE Hotel__c.Name != NULL].Rooms__r;

        List<Room__c> roomsReturned = [SELECT Name FROM Room__c WHERE Hotel__r.Name LIKE :stringToSearch AND Hotel__r.Is_Active__c = TRUE];
        return roomsReturned;

    }
}
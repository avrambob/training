/**
 * Created by avram on 06-May-20.
 */

public with sharing class TestDbOpeations {

    public static void insertIntoDb() {
        List<Contact> conList = new List<Contact>{
                new Contact(FirstName = 'Joe', LastName = 'Smith', Department = 'Finance'),
                new Contact(FirstName = 'Kathy', LastName = 'Smith', Department = 'IT'),
                new Contact()
        };

        //bulk insert all contacts with one DML call

        Database.SaveResult[] srList = Database.insert(conList, false);
        System.debug('srList = ' + srList);

        for(Database.SaveResult sr : srList){
            if(sr.isSuccess()){
                // operation was successfull, so get the ID of the record that was processed
                System.debug('Successful inserted contact. Contact ID: ' + sr.getId());
            }else{
                //operation failed, so get all errors
                for(Database.Error err:sr.getErrors()){
                    System.debug('The following error has occured,');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Contact fields that affected thie error: ' + err.fields);
                }
            }
        }
    }
}

/*TestDbOpeations.insertIntoDb();

20:53:36.49 (514414152)|USER_DEBUG|[17]|DEBUG|srList = (Database.SaveResult[getErrors=();getId=0032o00002n3ZEiAAM;isSuccess=true;],
 Database.SaveResult[getErrors=();getId=0032o00002n3ZEjAAM;isSuccess=true;],
 Database.SaveResult[getErrors=(Database.Error[getFields=(LastName);getMessage=Required fields are missing: [LastName];getStatusCode=REQUIRED_FIELD_MISSING;]);getId=null;isSuccess=false;])
20:53:36.49 (515036908)|USER_DEBUG|[22]|DEBUG|Successful inserted contact. Contact ID: 0032o00002n3ZEiAAM
20:53:36.49 (515226864)|USER_DEBUG|[22]|DEBUG|Successful inserted contact. Contact ID: 0032o00002n3ZEjAAM
20:53:36.49 (516517049)|USER_DEBUG|[26]|DEBUG|The following error has occured,
20:53:36.49 (529601062)|USER_DEBUG|[27]|DEBUG|REQUIRED_FIELD_MISSING: Required fields are missing: [LastName]
20:53:36.49 (529724682)|USER_DEBUG|[28]|DEBUG|Contact fields that affected thie error: (LastName) */
/**
 * Created by avram on 26-Jan-21.
 */

@IsTest
private class DailyLeadProcessorTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    @IsTest
    static void testScheduledTask() {

     List<Lead> myList = new List<Lead>();
     for(Integer i=1;i<=200;i++){
         myList.add(new Lead(LastName='LeadName ' + i ,Company='Company '+ i));
     }
     insert myList;

        System.Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
                CRON_EXP,
                new DailyLeadProcessor());
        System.Test.stopTest();

    }
}
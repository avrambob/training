/**
 * Created by avram on 24-May-20.
 */

public with sharing class AddPrimaryContact implements Queueable{

    private Contact myContact;
    private String stat;

    public AddPrimaryContact(Contact myContact, String stat){
        this.myContact = myContact;
        this.stat = stat;
    }

    public void execute(QueueableContext myQc) {

        List<Contact> contactList = new List<Contact>();
        for(Account acct :[SELECT Id,BillingState from Account WHERE BillingState =:stat LIMIT 200]){
            Contact c = myContact.clone();
            c.AccountId = acct.Id;
            contactList.add(c);
            //contactList.add(new Contact(myContact.clone()));
        }
    if(contactList.size() > 0){
        insert contactList;
    }

    }
}

/*Create an Apex class called 'AddPrimaryContact' that implements the Queueable interface.
Create a constructor for the class that accepts as its first argument a Contact sObject and a second argument as a string for the State abbreviation.
The execute method must query for a maximum of 200 Accounts with the BillingState specified by the State abbreviation passed into the
constructor and insert the Contact sObject record associated to each Account. Look at the sObject clone() method.
Create an Apex test class called 'AddPrimaryContactTest'.
In the test class, insert 50 Account records for BillingState "NY" and 50 Account records for BillingState "CA". Create an instance of the
AddPrimaryContact class, enqueue the job and assert that a Contact record was inserted for each of the 50 Accounts with the BillingState of "CA".
The unit tests must cover all lines of code included in the AddPrimaryContact class, resulting in 100% code coverage.
Run your test class at least once (via 'Run All' tests the Developer Console) before attempting to verify this challenge.*/
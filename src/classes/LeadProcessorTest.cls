/**
 * Created by avram on 23-May-20.
 */

@IsTest
private class LeadProcessorTest {
    @IsTest
    static void testBehavior() {
        List<Lead> leadList = new List<Lead>();
        for(Integer i=0;i<200;i++){
            Lead l = new Lead();
            l.FirstName = 'FirstName' + i;
            l.LastName = 'LastName' + i;
            l.Company = 'Company' + i;
            leadList.add(l);
        }

        insert leadList;
        System.Test.startTest();

        LeadProcessor obj = new LeadProcessor();
        Id batchId = Database.executeBatch(obj);

        System.test.stopTest();
    }
}
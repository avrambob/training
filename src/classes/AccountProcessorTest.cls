/**
 * Created by avram on 21-May-20.
 */

@IsTest
private class AccountProcessorTest {
    @IsTest
    static void testBehavior() {
        Account a = new Account(Name='test nr contacts');
        insert a;
        Contact cont = New Contact();
        cont.FirstName ='Bob';
        cont.LastName ='Masters';
        cont.AccountId = a.Id;
        Insert cont;
        List<Id> acId = new List<Id>{a.Id};
        System.Test.startTest();

        AccountProcessor.countContacts(acId);

        System.test.stopTest();
    }
}
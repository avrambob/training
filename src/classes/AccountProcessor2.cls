/**
 * Created by avram on 27-Feb-21.
 */

public with sharing class AccountProcessor2 {

    @Future
    public static void countContacts2(List<Id> IdList){
        List<Account> accList = [SELECT Id,Number_of_Contacts__c,(SELECT Id FROM Contacts) FROM Account WHERE Id IN :IdList];
        List<Account> updatedAccounts = new List<Account>();
        for(Account a : accList){
            a.Number_of_Contacts__c = a.Contacts.size();
            updatedAccounts.add(a);
        }

        if(updatedAccounts.size() > 0)
            update updatedAccounts;
    }

}

//Create a field on the Account object called 'Number_of_Contacts__c' of type Number. This field will hold the total number of Contacts for the Account.
//Create an Apex class called 'AccountProcessor' that contains a 'countContacts' method that accepts a List of Account IDs. This method must use the @future //annotation.
//For each Account ID passed to the method, count the number of Contact records associated to it and update the 'Number_of_Contacts__c' field with this value.
//Create an Apex test class called 'AccountProcessorTest'.
//The unit tests must cover all lines of code included in the AccountProcessor class, resulting in 100% code coverage.
//Run your test class at least once (via 'Run All' tests the Developer Console) before attempting to verify this challenge.
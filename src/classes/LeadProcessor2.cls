/**
 * Created by avram on 27-Feb-21.
 */

public with sharing class LeadProcessor2 implements Database.Batchable<sObject>{
    public Integer recordsProcessed= 0;
    public Database.QueryLocator start(Database.BatchableContext bc) {

        return Database.getQueryLocator('SELECT Id, LeadSource FROM Lead');
    }

    public void execute(Database.BatchableContext bc, List<Lead> leadIds) {
        List<Lead> updatedLeads = new List<Lead>();
        for(Lead l : leadIds){
            if(l.LeadSource != 'Dreamforce'){
                l.LeadSource = 'Dreamforce';
                updatedLeads.add(l);
                recordsProcessed++;
            }
        }
        if (updatedLeads.size() > 0)
            update updatedLeads;
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('recordsProcessed ' + recordsProcessed);
        AsyncApexJob job = [SELECT Id,Status,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email from AsyncApexJob WHERE Id = :bc.getJobId()];
    }


}

//Create an Apex class called 'LeadProcessor' that uses the Database.Batchable interface.
//Use a QueryLocator in the start method to collect all Lead records in the org.
//The execute method must update all Lead records in the org with the LeadSource value of 'Dreamforce'.
//Create an Apex test class called 'LeadProcessorTest'.
//In the test class, insert 200 Lead records, execute the 'LeadProcessor' Batch class and test that all Lead records were updated correctly.
//The unit tests must cover all lines of code included in the LeadProcessor class, resulting in 100% code coverage.
//Run your test class at least once (via 'Run All' tests the Developer Console) before attempting to verify this challenge.
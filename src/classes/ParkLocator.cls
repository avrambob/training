/**
 * Created by avram on 07-Feb-21.
 */

public with sharing class ParkLocator {
    public static String[] country(String countryName){
        ParkService.ParksImplPort p = new ParkService.ParksImplPort();
        String[] reponse = p.byCountry(countryName);
        return reponse;
    }
}
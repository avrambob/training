/**
 * Created by avram on 24-May-20.
 */

@IsTest
private class AddPrimaryContactTest {
    @IsTest
    static void testBehavior() {
     // In the test class, insert 50 Account records for BillingState "NY" and 50 Account records for BillingState "CA".

        List<Account> accList = new List<Account>();
        for(Integer i=0;i<50;i++){
            accList.add(new Account(Name='Name' + i,BillingState = 'NY'));
        }
        for(Integer i=50;i<100;i++){
            accList.add(new Account(Name='Name' + i, BillingState = 'CA'));
        }
        if(accList.size() > 0){
            insert accList;
        }

        Contact cont = new Contact(FirstName = 'MyTest', LastName='Contact');
        String state = 'CA';
        AddPrimaryContact updater = new AddPrimaryContact(cont, state);

        System.test.startTest();
        System.enqueueJob(updater);
        system.Test.stopTest();
    }
}
/**
 * Created by avram on 21-May-20.
 */

public with sharing class AccountProcessor {

    @Future
    public static void countContacts(List<Id> acctsIds){

        List<Account> acctList = [Select Id,Number_of_Contacts__C, (select id from contacts) from Account WHERE Id IN :acctsIds];
        //System.debug('acctList = ' + acctList);
        List<Account> updatedAcctList = new List<Account>();
        for(Account acct: acctList){
            acct.Number_of_Contacts__C = acct.Contacts.size();
            updatedAcctList.add(acct);
        }
        if(updatedAcctList.size() > 0) {
            update updatedAcctList;
        }
        //System.debug(updatedAcctList);
    }

}
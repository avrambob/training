/**
 * Created by avram on 10-Feb-21.
 */

@IsTest
private class AccountManagerTest {
    @IsTest
    static void testBehavior() {
        Id recordId = createTestRecord();
        RestRequest request= new RestRequest();
        request.requestURI = 'https://eu25.salesforce.com/services/apexrest/Accounts/' + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account acct = AccountManager.getAccount();
    }

    //helper method
    static Id createTestRecord(){
        Account a = new Account(Name='Test Record ws');
        insert a;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=1;i<3;i++){
            Contact c = new Contact(LastName='Test Record ws ' + i,AccountId = a.Id);
            contactList.add(c);
        }
        insert contactList;

        return a.Id;
    }
}
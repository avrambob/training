/**
 * Created by avram on 23-May-20.
 */

public with sharing class LeadProcessor implements Database.Batchable<sObject>,Database.Stateful{
    public Integer recordsProcessed = 0;

    public Iterable<SObject> start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT ID,LeadSource from Lead');
    }

    public void execute(Database.BatchableContext bc, List<Lead> scope) {
        List<Lead> updatedLeadList = new List<Lead>();
        for(Lead l : scope){
            l.LeadSource = 'Dreamforce';
            updatedLeadList.add(l);
        }
        update updatedLeadList;
    }

    public void finish(Database.BatchableContext bc) {
        System.debug(recordsProcessed + ' records processed. Shazam!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,JobItemsProcessed,TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob
                            WHERE Id = :bc.getJobId()];
    }
}

/*Create an Apex class called 'LeadProcessor' that uses the Database.Batchable interface.
Use a QueryLocator in the start method to collect all Lead records in the org.
The execute method must update all Lead records in the org with the LeadSource value of 'Dreamforce'.
Create an Apex test class called 'LeadProcessorTest'.
In the test class, insert 200 Lead records, execute the 'LeadProcessor' Batch class and test that all Lead records were updated correctly.
The unit tests must cover all lines of code included in the LeadProcessor class, resulting in 100% code coverage.
Run your test class at least once (via 'Run All' tests the Developer Console) before attempting to verify this challenge.*/
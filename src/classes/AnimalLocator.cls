/**
 * Created by avram on 03-Feb-21.
 */

public with sharing class AnimalLocator {

    public static String getAnimalNameById (Integer id){
        String animalName;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/' + id);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
// deserializare json -> DEBUG|{animal={eats=chicken food, id=1, name=chicken, says=cluck cluck}}
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
// insereaza intr-o mapa lista de valori ->  DEBUG|{eats=chicken food, id=1, name=chicken, says=cluck cluck}
            Map<String, Object> finalResults = (Map<String, Object>)results.get('animal');
            animalName = String.valueOf(finalResults.get('name')); // sau animalName = (String)finalResults.get('name')
        }
        return  animalName;
    }
}
//Create an Apex class:
//Name: AnimalLocator
//Method name: getAnimalNameById
//The method must accept an Integer and return a String.
//The method must call https://th-apex-http-callout.herokuapp.com/animals/id, using the ID passed into the method
//The method returns the value of the name property (i.e., the animal name)
//Create a test class:
//Name: AnimalLocatorTest
//The test class uses a mock class called AnimalLocatorMock to mock the callout response
//Create unit tests:
//Unit tests must cover all lines of code included in the AnimalLocator class, resulting in 100% code coverage
//Run your test class at least once (via Run All tests the Developer Console) before attempting to verify this challenge
/**
 * Created by avram on 06-May-20.
 */

public with sharing class TestList {
    public static void readChildRelation(){

        List<Account> acctsWithContacts = [SELECT Name,(SELECT FirstName,LastName from Contacts)
                                       FROM Account WHERE Phone <> null LIMIT 5];

        //get Child records
        //List<Contact> cts = new List<Contact>();
        for(Integer i=0;i<acctsWithContacts.size();i++){
            String contactStrings = '';
            for(Integer j=0;j<acctsWithContacts.get(i).Contacts.size();j++) {
                contactStrings += acctsWithContacts.get(i).Contacts.get(j).LastName + ' ' + acctsWithContacts.get(i).Contacts.get(j).FirstName +  ' ; ' ;
            }
            System.debug('Account: ' + acctsWithContacts.get(i).Name + ' are conturile asociate ' + ' contacte : ' + contactStrings);
        }
    }
}

/* TestList.readChildRelation();
22:03:45.42 (94167394)|USER_DEBUG|[18]|DEBUG|Account: Acme are conturile asociate  contacte :
22:03:45.42 (96368298)|USER_DEBUG|[18]|DEBUG|Account: GenePoint are conturile asociate  contacte : Frank Edna ;
22:03:45.42 (98577823)|USER_DEBUG|[18]|DEBUG|Account: United Oil & Gas, UK are conturile asociate  contacte : James Ashley ;
22:03:45.42 (104094775)|USER_DEBUG|[18]|DEBUG|Account: United Oil & Gas, Singapore are conturile asociate  contacte : Ripley Tom ; D'Cruz Liz ;
22:03:45.42 (107041162)|USER_DEBUG|[18]|DEBUG|Account: Edge Communications are conturile asociate  contacte : Gonzalez Rose ; Forbes Sean ; */
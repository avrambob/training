/**
 * Created by avram on 27-Feb-21.
 */
@IsTest
public with sharing class AddPrimaryContact2Test {
    

    @IsTest
    static void testBehaviour(){

        List<Account> acList = new List<Account>();
        for(Integer i =1 ;i< 51; i++){
            Account a = new Account(Name = 'Account ww ' + i,BillingState = 'NY');
            acList.add(a);
        }
        for(Integer i =51 ;i< 101; i++){
            Account a = new Account(Name = 'Account ww' + i,BillingState = 'CA');
            acList.add(a);
        }

        if(acList.size() > 0)
            insert acList;


        Contact c = new Contact(FirstName = 'FirstNamew',LastName = 'LastNamew');
        //insert c;
        String state = 'CA';

        Test.startTest();
        AddPrimaryContact2 jobToRun = new AddPrimaryContact2(c,state);
        Id jobId = System.enqueueJob(jobToRun);
        test.stopTest();
    }
}


//In the test class, insert 50 Account records for BillingState "NY" and 50 Account records for BillingState "CA". Create an instance of the
// AddPrimaryContact class, enqueue the job and assert that a Contact record was inserted for each of the 50 Accounts with the BillingState of "CA".
/**
 * Created by avram on 12-May-20.
 */
@IsTest
public with sharing class TestVerifyDate {
    static testMethod void testCheckDates(){
        Date d1 = Date.newInstance(2020,06,01);
        Date d2 = Date.newInstance(2020,06,30);
        System.Test.startTest();
        VerifyDate.CheckDates(d1,d2);
        System.test.stopTest();
        System.assertEquals(VerifyDate.CheckDates(d1,d2),Date.newInstance(2020,06,30));
    }

    static testMethod void testCheckDates2(){
        Date d1 = Date.newInstance(2020,06,01);
        Date d2 = Date.newInstance(2020,08,30);
        System.Test.startTest();
        VerifyDate.CheckDates(d1,d2);
        System.test.stopTest();
        System.assertEquals(VerifyDate.CheckDates(d1,d2),Date.newInstance(2020,06,30));


    }
}